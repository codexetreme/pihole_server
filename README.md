# PiHole Docker made to run on any system

-run `./sd` in the root level to start the docker


## Customization
set your own password by setting the `WEBPASSWORD` env variable
set your `ServerIP` to your devices ip, on linux use `ip addr` and check your ip for the correct interface


## Dependencies
- docker
- docker-compose
